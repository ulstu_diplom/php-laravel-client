<?php

namespace Forecast\Models;

use Illuminate\Database\Eloquent\Model;

class TimeSeriesPrediction extends Model
{
    private $apiUrl = "api.greamko.ru";

    public function prediction($method, $ts)
    {
        $url = $this->apiUrl . '/' . $method;

        $ch = curl_init($url);

        $arrPost = array(
            "ts" 		=>	$ts,
            "version"	=>	"0.0.2",
            "horizon"	=>	12,
//            "frequency"	=>	3,
//            "factors"	=>	array(1, 0, 0),
//            "sfactors"	=>	array(1, 3, 1)
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrPost));

        $response = curl_exec($ch);

        return $response;
    }
}
