<?php

namespace Forecast\Models;

use Illuminate\Database\Eloquent\Model;

class TimeSeries extends Model
{
    protected $table = 'timeseries';

    public function updateAll()
    {
        $path = storage_path('app/ts/');

        $files = array_values(array_filter(scandir($path), function($file) {
            return !is_dir($file);
        }));

        array_multisort($files, SORT_ASC, SORT_NATURAL);
        $data = array();
        foreach($files as $file){
            $csvFile = file($path . $file);
            $data[$file] = [];
            foreach ($csvFile as $line) {
                $data[$file][] = str_getcsv($line, ';');
            }
        }
        $finalData = [];
        foreach($data as $file => $fileData){
            $valueIndex = 0;
            foreach($fileData as $key => $value){
                if($key==0) {
                    $valueIndex = array_search('value', $value);
                }
                else{
                    $finalData[$file][] = $value[$valueIndex];
                }
            }
        }

        foreach($finalData as $file => $fileData){
            $model = new TimeSeries();
            $model->name = explode('.',$file)[0];
            $model->ts_data = json_encode($fileData);
            $model->save();
        }

        return $finalData;
    }
}
