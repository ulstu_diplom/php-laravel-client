<?php

namespace Forecast\Http\Controllers;

use Forecast\Models\TimeSeries;
use Forecast\Models\TimeSeriesPrediction;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Forecast\Http\Controllers\Controller;

class TsController extends Controller
{
    public function updateAll()
    {
        $timeSeries = new TimeSeries();
        $tsAll = TimeSeries::all();
        foreach ($tsAll as $ts) {
            $ts->delete();
        }
        $data = $timeSeries->updateAll();
        return ($data);
    }

    public function getAll()
    {
        $data = TimeSeries::all();
        return ($data);
    }

    public function predict(Request $request)
    {
        if (!$request->ajax()) {
            return null;
        }

        $data = $request->all();

        if (empty($data['tsId']) || empty($data['predictionType'])) {
            return null;
        }
        $result = TimeSeries::where('id', $data['tsId'])->get();

        $ts = json_decode($result[0]['ts_data']);
        $timeSeriesPrediction = new TimeSeriesPrediction();

        $predictionResult = json_decode($timeSeriesPrediction->prediction($data['predictionType'], $ts), true);

        foreach ($ts as $key => $data) {
            $predictionResult{'dygraphFormat'}['predicted'][] = [
                $key + 1,
                intval($data),
                intval($predictionResult['data']['fitted'][$key])
            ];
        }

        foreach ($predictionResult['data']['mean'] as $key => $data) {
            $predictionResult{'dygraphFormat'}['predicted'][] = [
                count($predictionResult['data']['fitted']) + $key + 1,
                null,
                intval($data),
            ];
        }


        return view('ajax.ts', compact('predictionResult'));
    }
}
