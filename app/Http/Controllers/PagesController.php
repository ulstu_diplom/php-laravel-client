<?php namespace Forecast\Http\Controllers;

use Forecast\Http\Controllers\Controller;
use Forecast\Models\TimeSeries;

class PagesController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function home()
    {
        return view('pages.home');
    }

    public function predict()
    {
        $ts = TimeSeries::all();

        $arrTs = [];
        foreach ($ts as $tsValue) {
            $arrTs[$tsValue['name']] = $tsValue['id'];
        }

        return view('pages.predict', compact('arrTs'));
    }

}