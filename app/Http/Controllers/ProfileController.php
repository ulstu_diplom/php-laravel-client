<?php

namespace Forecast\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Forecast\Http\Requests;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $viewData = ['user' => Auth::user()];
        return view('pages.profile.index', $viewData);
    }
}
