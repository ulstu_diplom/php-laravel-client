<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="{{ asset('bower/jquery/dist/jquery.min.js') }}"></script>
<link href="{{ asset('bower/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<script src="{{ asset('bower/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('bower/dygraphs/dygraph-combined.js') }}"></script>

<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
<script src="{{ asset('js/script.js') }}"></script>

<title>@yield('title')</title>