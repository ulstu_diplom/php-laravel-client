@extends('layouts.master')

@section('title', 'Time Series Prediction')

@section('head', 'Прогнозирование временных рядов')

{{--@section('annotation')--}}
{{--Use this document as a way to quickly start any new project.<br> All you get is this text and a--}}
{{--mostly barebones HTML document.--}}
{{--@stop--}}

@section('content')
    <form id="tsForm">
        <div class="form-group ts-settings-form">
            <div class="controllerBlock">
                <label for="tsSelect">Choose ts to predict</label>
                <select class="form-control" id="tsSelect" name="tsId">
                    @foreach($arrTs as $key => $value)
                        <option value="{{$value}}">{{$key}}</option>
                    @endforeach
                </select>
            </div>
            <div class="controllerBlock">
                <label for="predictionTypeSelect">Choose model</label>
            <select class="form-control" id="predictionTypeSelect" name="predictionType">
                <option value="arima">Arima</option>
                <option value="ets">ETS</option>
            </select>
            </div>
        </div>
        <button class="btn btn-default" id="tsFormSubmit" type="button">Обновить</button>
    </form>
    <div id="tsPredictionResult">

    </div>
@stop