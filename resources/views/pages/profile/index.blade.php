@extends('layouts.profile')

@section('title', 'Time Series Prediction')

@section('content')
    <div class="container-fluid well">
        <div class="row-fluid">

            <div class="col-xs-12">
                <h3>{{$user->name}}</h3>
                <h6>Email: {{$user->email}}</h6>
            </div>

        </div>
    </div>
@stop