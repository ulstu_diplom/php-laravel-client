<h3>{{$predictionResult['data']['method'][0]}}</h3>
<div id="graphdiv"></div>
<script type="text/javascript">
    g = new Dygraph(
            // containing div
            document.getElementById("graphdiv"),
            {{json_encode($predictionResult['dygraphFormat']['predicted'])}},
            {
                labels: ["x", "Original", "Predicted"],
                rollPeriod: 7
            }
    );
</script>
<pre>
{{print_r($predictionResult)}}
</pre>