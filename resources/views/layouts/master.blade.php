<html>
<head>
    @include('includes.head')
</head>
<body data-gr-c-s-loaded="true">

@include('includes.header')

<div class="main container-fluid">
    <div class="row-fluid">
        <div class="head">

            @if ($__env->yieldContent('head'))
                <h1>@yield('head')</h1>
            @endif

            @if ($__env->yieldContent('annotation'))
                <p class="lead">@yield('annotation')</p>
            @endif
        </div>

        @yield('content')

    </div>
    <footer class="row">
        @include('includes.footer')
    </footer>
</div>
</body>

</html>