<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
</head>
<body data-gr-c-s-loaded="true">

@include('includes.header')

<div class="main container-fluid">
    <div class="row-fluid">
        <div class="profile_sidebar col-xs-12 col-md-3">
            @include('includes.sidebar_profile')
        </div>
        <div class="col-xs-12 col-md-9">
            @yield('content')
        </div>
        <div class="clearfix"></div>

    </div>
    <footer class="row">
        @include('includes.footer')
    </footer>
</div>
</body>

</html>