$(document).ready(function () {

    $("#tsFormSubmit").click(function () {
        $.ajax({
            type: "POST",
            url: "/ts/predict",
            data: $("#tsForm").serialize()
        }).done(function (data) {
            $('#tsPredictionResult').html(data);
        });
    });

    $('a[href="' + this.location.pathname + '"]').parents('li,ul').addClass('active');
});